package sample;

import javafx.scene.control.Alert;

public class Extentions {

    public void showAlertWithoutHeaderText(String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText(text);
        alert.setHeaderText(null);
        alert.showAndWait();
    }
}
