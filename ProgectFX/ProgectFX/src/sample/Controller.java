package sample;

import Osnow.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;


import javax.swing.*;
import java.awt.event.ActionListener;

public class Controller {
    public Button bladebtn;
    public Label Pricelbl;
    public Button editbtn;
    public Button deletebtn;
    Add add = new Add();
    Edit edit = new Edit();

    HotelOfCat hotelOfCat = new HotelOfCat();

    public ListView<String> list = new ListView<>();
    public ObservableList<String> names = FXCollections.observableArrayList();

    public void AddButton(ActionEvent event) {
        add.AddObject(hotelOfCat, names);
        list.setItems(names);
        Pricelbl.setText(Double.toString(Math.round(hotelOfCat.GetIncome())));
    }

    public void EditButton(ActionEvent event) {
        try {
            int index;
            index = list.getSelectionModel().getSelectedIndex();

            if (hotelOfCat.GetListCats().get(index).getClass().equals(WildCats.class)) {
                WildCats wildCats = (WildCats) hotelOfCat.GetListCats().get(index);
                edit.EditAct(hotelOfCat, wildCats, list, names, index);
            }
            if (hotelOfCat.GetListCats().get(index).getClass().equals(FluffyCat.class)) {
                FluffyCat fluffyCat = (FluffyCat) hotelOfCat.GetListCats().get(index);
                edit.EditAct(hotelOfCat, fluffyCat, list, names, index);
            }
            if (hotelOfCat.GetListCats().get(index).getClass().equals(SmoothCat.class)) {
                SmoothCat smoothCat = (SmoothCat) hotelOfCat.GetListCats().get(index);
                edit.EditAct(hotelOfCat, smoothCat, list, names, index);
            }

            Pricelbl.setText(Double.toString(Math.round(hotelOfCat.GetIncome())));
        } catch (Exception e) {
            new Extentions().showAlertWithoutHeaderText("Please, choose any cat!");
        }
    }

    public void DeleteButton(ActionEvent event) {
        try {
            int index;
            index = list.getSelectionModel().getSelectedIndex();
            hotelOfCat.GetListCats().remove(index);
            names.remove(index);
            Pricelbl.setText(Double.toString(Math.round(hotelOfCat.GetIncome())));
        } catch (Exception exc) {
            new Extentions().showAlertWithoutHeaderText("Please, choose any cat!");
        }
    }
}





