package sample;

import Osnow.FluffyCat;
import Osnow.HotelOfCat;
import Osnow.SmoothCat;
import Osnow.WildCats;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hotel of Cats");
        primaryStage.setScene(new Scene(root, 460, 300));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);

    }
}
