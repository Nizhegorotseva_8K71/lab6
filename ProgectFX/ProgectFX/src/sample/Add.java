package sample;

import Osnow.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.event.ActionEvent;


import javax.swing.*;
import java.awt.event.ActionListener;

public class Add {

    public void AddObject(HotelOfCat hotelOfCat, ObservableList names) {

        JTextField nameField = new JTextField();
        JTextField ageField = new JTextField();
        JTextField weightField = new JTextField();
        JTextField HostilityField = new JTextField();
        JTextField FriendlinessField = new JTextField();
        JTextField LenghtField = new JTextField();
        JTextField ClothesField = new JTextField();

        HostilityField.setEditable(true);
        FriendlinessField.setEditable(false);
        LenghtField.setEditable(false);
        ClothesField.setEditable(false);

        ButtonGroup group = new ButtonGroup();
        JRadioButton wildR = new JRadioButton("Wild", true);
        group.add(wildR);
        JRadioButton fluffyR = new JRadioButton("Fluffy", false);
        group.add(fluffyR);
        JRadioButton smoothR = new JRadioButton("Smooth", false);
        group.add(smoothR);


        wildR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                HostilityField.setEditable(true);
                FriendlinessField.setEditable(false);
                LenghtField.setEditable(false);
                ClothesField.setEditable(false);

            }
        });
        fluffyR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                HostilityField.setEditable(false);
                FriendlinessField.setEditable(true);
                LenghtField.setEditable(true);
                ClothesField.setEditable(false);
            }
        });
        smoothR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                HostilityField.setEditable(false);
                FriendlinessField.setEditable(true);
                LenghtField.setEditable(false);
                ClothesField.setEditable(true);
            }
        });
        Object[] message = new Object[]{"", wildR, "", fluffyR, "", smoothR, "Name", nameField, "Age", ageField, "Weight", weightField, "Hostility", HostilityField, "Friendliness", FriendlinessField, "Lenght", LenghtField, "Clothes", ClothesField};
        int option = JOptionPane.showConfirmDialog(null, message,
                "Create new cat", JOptionPane.OK_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            String name = nameField.getText();
            double age = Double.parseDouble(ageField.getText());
            double weight = Double.parseDouble(weightField.getText());

            if (wildR.isSelected()) {
                double hostility = Double.parseDouble(HostilityField.getText());
                WildCats wild = new WildCats(name, age, weight, hostility);
                hotelOfCat.AddCat(wild);
                names.add("Wild Cat: " + wild.GetName() + "; Cost: " + Double.toString(wild.GetCost()));
            }
            if (fluffyR.isSelected()) {
                double friendliness = Double.parseDouble(FriendlinessField.getText());
                double lenght = Double.parseDouble(LenghtField.getText());
                FluffyCat fluffyCat = new FluffyCat(name, age, weight, friendliness, lenght);
                hotelOfCat.AddCat(fluffyCat);
                names.add("Fluffy cat: " + fluffyCat.GetName() + "; Cost: " + Double.toString(fluffyCat.GetCost()));
            }
            if (smoothR.isSelected()) {
                double friendliness = Double.parseDouble(FriendlinessField.getText());
                double clothes = Double.parseDouble(ClothesField.getText());
                SmoothCat smoothCat = new SmoothCat(name, age, weight, friendliness, clothes);
                hotelOfCat.AddCat(smoothCat);
                names.add("Smooth Cat: " + smoothCat.GetName() + "; Cost: " + Double.toString(smoothCat.GetCost()));
            }

        } catch (Exception exc) {
            new Extentions().showAlertWithoutHeaderText("Enter correct values, please!");

        }
    }

}
