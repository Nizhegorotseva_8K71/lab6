package sample;

import Osnow.*;
import javafx.collections.ObservableList;

import javax.swing.*;


public class Edit {

    public void  EditAct(HotelOfCat hotelOfCat, WildCats wildCats, javafx.scene.control.ListView list, ObservableList names, int index){

        JTextField nameField = new JTextField(wildCats.GetName());
        JTextField WeightField = new JTextField(Double.toString(wildCats.GetWeight()));
        JTextField AgeField = new JTextField(Double.toString(wildCats.GetAge()));
        JTextField HostilityField = new JTextField(Double.toString(wildCats.GetHostility()));

        Object[] message = new Object[]{ "Name", nameField,
                "Weight", WeightField, "Age", AgeField, "Hostility", HostilityField};
        int option = JOptionPane.showConfirmDialog(null, message,
                "Edit Cat", JOptionPane.OK_CANCEL_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            wildCats.SetName(nameField.getText());
            wildCats.SetCost(Double.parseDouble(AgeField.getText()),Double.parseDouble(WeightField.getText()), Double.parseDouble(HostilityField.getText()));
            wildCats.SetAge(Double.parseDouble(AgeField.getText()));
            wildCats.SetWeight(Double.parseDouble(WeightField.getText()));

            list.setItems(names);
            names.set(index, "Wild Cat: " + wildCats.GetName() + "; Cost: " + Double.toString(wildCats.GetCost()));


        } catch (Exception exc) {
            new Extentions().showAlertWithoutHeaderText("Enter correct values, please!");
        }


    }
    public void EditAct(HotelOfCat hotelOfCat, FluffyCat fluffyCat, javafx.scene.control.ListView list, ObservableList names, int index){
        JTextField nameField = new JTextField(fluffyCat.GetName());
        JTextField AgeField = new JTextField(Double.toString(fluffyCat.GetAge()));
        JTextField WeightField = new JTextField(Double.toString(fluffyCat.GetWeight()));
        JTextField FrienlinessField = new JTextField(Double.toString(fluffyCat.GetFriendliness()));
        JTextField LenghtField = new JTextField(Double.toString(fluffyCat.GetLenght()));

        Object[] message = new Object[]{ "Name", nameField,
                "Weight", WeightField, "Age", AgeField, "Frienliness", FrienlinessField, "lenght", LenghtField};
        int option = JOptionPane.showConfirmDialog(null, message,
                "Edit Cat", JOptionPane.OK_CANCEL_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            fluffyCat.SetName(nameField.getText());
            fluffyCat.SetCost(Double.parseDouble(AgeField.getText()),Double.parseDouble(WeightField.getText()),Double.parseDouble(FrienlinessField.getText()) , Double.parseDouble(LenghtField.getText()));
            fluffyCat.SetAge(Double.parseDouble(AgeField.getText()));
            fluffyCat.SetWeight(Double.parseDouble(WeightField.getText()));
            fluffyCat.SetFriendliness(Double.parseDouble(FrienlinessField.getText()));
            fluffyCat.SetLenght(Double.parseDouble(LenghtField.getText()));

            list.setItems(names);
            names.set(index, "Fluffy Cat: " + fluffyCat.GetName() + "; Cost: " + Double
                    .toString(fluffyCat.GetCost()));


        } catch (Exception exc) {
            new Extentions().showAlertWithoutHeaderText("Enter correct values, please!");
        }
    }
    public void EditAct(HotelOfCat hotelOfCat, SmoothCat smoothCat, javafx.scene.control.ListView list, ObservableList names, int index){
        JTextField nameField = new JTextField(smoothCat.GetName());
        JTextField AgeField = new JTextField(Double.toString(smoothCat.GetAge()));
        JTextField WeightField = new JTextField(Double.toString(smoothCat.GetWeight()));
        JTextField FrienlinessField = new JTextField(Double.toString(smoothCat.GetFriendliness()));
        JTextField ClothesField = new JTextField(Double.toString(smoothCat.GetClothes()));

        Object[] message = new Object[]{ "Name", nameField,
                "Age", AgeField, "Weight", WeightField, "Friendliness", FrienlinessField,"Clothes", ClothesField};
        int option = JOptionPane.showConfirmDialog(null, message,
                "Edit Cat", JOptionPane.OK_CANCEL_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            smoothCat.SetName(nameField.getText());
            smoothCat.SetCost(Double.parseDouble(AgeField.getText()),Double.parseDouble(WeightField.getText()),Double.parseDouble(FrienlinessField.getText()) , Double.parseDouble(ClothesField.getText()));
            smoothCat.SetAge(Double.parseDouble(AgeField.getText()));
            smoothCat.SetWeight(Double.parseDouble(WeightField.getText()));
            smoothCat.SetFriendliness(Double.parseDouble(FrienlinessField.getText()));
            smoothCat.SetClothes(Double.parseDouble(ClothesField.getText()));

            list.setItems(names);
            names.set(index, "smoothCat: " + smoothCat.GetName() + "; Cost: " + Double.toString(smoothCat.GetCost()));


        } catch (Exception exc) {
            new Extentions().showAlertWithoutHeaderText("Enter correct values, please!");
        }
    }


}

