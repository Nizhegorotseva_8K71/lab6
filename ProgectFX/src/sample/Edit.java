package sample;

import Logic.BladeWeapon;
import Logic.Division;
import Logic.ShotGun;
import Logic.Sniper;
import javafx.collections.ObservableList;

import javax.swing.*;


public class Edit {

    public void  EditAct(Division division, BladeWeapon bladeWeapon, javafx.scene.control.ListView list, ObservableList names, int index){
        JTextField nameField = new JTextField(bladeWeapon.GetName());
        JTextField CostField = new JTextField(Integer.toString(bladeWeapon.GetCost()));
        JTextField SharpField = new JTextField(Integer.toString(bladeWeapon.GetSharp()));
        JTextField LengthField = new JTextField(Integer.toString(bladeWeapon.GetLength()));
        Object[] message = new Object[]{ "Name", nameField, "Cost", CostField,
                "Sharp", SharpField, "Length", LengthField};
        int option = JOptionPane.showConfirmDialog(null, message,
                "Edit Weapon", JOptionPane.OK_CANCEL_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            bladeWeapon.SetName(nameField.getText());
            bladeWeapon.SetCost(Integer.parseInt(CostField.getText()));
            bladeWeapon.SetSharp(Integer.parseInt(SharpField.getText()));
            bladeWeapon.SetLength(Integer.parseInt(LengthField.getText()));
            list.setItems(names);
            names.set(index, "Blade: " + bladeWeapon.GetName() + "; Cost: " + Integer.toString(bladeWeapon.GetCost()));
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(null, "Enter correct values, please!");
            EditAct(division, bladeWeapon, list, names, index);
        }

    }
    public void EditAct(Division division, Sniper sniper, javafx.scene.control.ListView list, ObservableList names, int index){
        JTextField nameField = new JTextField(sniper.GetName());
        JTextField CostField = new JTextField(Integer.toString(sniper.GetCost()));
        JTextField SpeedField = new JTextField(Integer.toString(sniper.GetSpeed()));
        JTextField DistanceField = new JTextField(Integer.toString(sniper.GetDistance()));
        JTextField PatronField = new JTextField(Integer.toString(sniper.GetPatron()));
        JTextField IncreaseField = new JTextField(Integer.toString(sniper.GetIncrease()));
        Object[] message = new Object[]{ "Name", nameField, "Cost", CostField,
                "Speed", SpeedField, "Distance", DistanceField, "Patron", PatronField,"Increase", IncreaseField};
        int option = JOptionPane.showConfirmDialog(null, message,
                "Edit Weapon", JOptionPane.OK_CANCEL_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            sniper.SetName(nameField.getText());
            sniper.SetCost(Integer.parseInt(CostField.getText()));
            sniper.SetSpeed(Integer.parseInt(SpeedField.getText()));
            sniper.SetDistance(Integer.parseInt(DistanceField.getText()));
            sniper.SetPatron(Integer.parseInt(PatronField.getText()));
            sniper.SetIncrease(Integer.parseInt(IncreaseField.getText()));
            list.setItems(names);
            names.set(index, "Sniper: " + sniper.GetName() + ": " + Integer.toString(sniper.GetCost()));
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(null, "Enter correct values, please!");
            EditAct(division, sniper, list, names, index);
        }
    }
    public void EditAct(Division division, ShotGun shotGun, javafx.scene.control.ListView list, ObservableList names, int index){
        JTextField nameField = new JTextField(shotGun.GetName());
        JTextField CostField = new JTextField(Integer.toString(shotGun.GetCost()));
        JTextField SpeedField = new JTextField(Integer.toString(shotGun.GetSpeed()));
        JTextField DistanceField = new JTextField(Integer.toString(shotGun.GetDistance()));
        JTextField PatronField = new JTextField(Integer.toString(shotGun.GetPatron()));
        JTextField ScatterField = new JTextField(Integer.toString(shotGun.GetScatter()));
        Object[] message = new Object[]{ "Name", nameField, "Cost", CostField,
                "Speed", SpeedField, "Distance", DistanceField, "Patron", PatronField,"Scatter", ScatterField};
        int option = JOptionPane.showConfirmDialog(null, message,
                "Edit Weapon", JOptionPane.OK_CANCEL_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            shotGun.SetName(nameField.getText());
            shotGun.SetCost(Integer.parseInt(CostField.getText()));
            shotGun.SetSpeed(Integer.parseInt(SpeedField.getText()));
            shotGun.SetDistance(Integer.parseInt(DistanceField.getText()));
            shotGun.SetPatron(Integer.parseInt(PatronField.getText()));
            shotGun.SetScatter(Integer.parseInt(ScatterField.getText()));
            list.setItems(names);
            names.set(index, "ShotGun: " + shotGun.GetName() + ": " + Integer.toString(shotGun.GetCost()));

        } catch (Exception exc) {
            JOptionPane.showMessageDialog(null, "Enter correct values, please!");
            EditAct(division, shotGun, list, names, index);
        }
    }


}

