package sample;

//import Logic.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {
    public Button bladebtn;
    public Label Pricelbl;
    public Button editbtn;
    public Button deletebtn;
    Add add = new Add();
    Edit edit = new Edit();
    public Division division= new Division();
    public ListView<String> list = new ListView<>();
    public ObservableList<String> names = FXCollections.observableArrayList();

    public void AddButton(ActionEvent event) {

        add.AddObject(division,names);
        list.setItems(names);
        Pricelbl.setText(Integer.toString(division.GetPrice()));
    }

    public void EditButton(ActionEvent event) {
        try {
            int index;
            index = list.getSelectionModel().getSelectedIndex();

            if (division.GetArrayList().get(index).getClass().equals(BladeWeapon.class)) {
                BladeWeapon bladeWeapon = (BladeWeapon) division.GetArrayList().get(index);
                edit.EditAct(division, bladeWeapon, list, names, index);
            }
            if (division.GetArrayList().get(index).getClass().equals(Sniper.class)) {
                Sniper sniper = (Sniper) division.GetArrayList().get(index);
                edit.EditAct(division, sniper, list, names, index);
            }
            if (division.GetArrayList().get(index).getClass().equals(ShotGun.class)) {
                ShotGun shotGun = (ShotGun) division.GetArrayList().get(index);
                edit.EditAct(division, shotGun, list, names, index);
            }

            Pricelbl.setText(Integer.toString(division.GetPrice()));
        }
        catch (Exception exc)
        {
            JOptionPane.showMessageDialog(null, "Please, choose any object!");
        }
    }


    public void DeleteButton(ActionEvent event){
        try {
            int index;
            index = list.getSelectionModel().getSelectedIndex();
            division.GetArrayList().remove(index);
            names.remove(index);
            Pricelbl.setText(Integer.toString(division.GetPrice()));
       }
       catch (Exception exc)
       {
          JOptionPane.showMessageDialog(null, "Please, choose any object!");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        BladeWeapon bladeWeapon = new BladeWeapon("StarBlade", 40, 2, 20);
        division.AddWeapon(bladeWeapon);
        names.add("Blade: " + bladeWeapon.GetName() + "; Cost: " + Integer.toString(bladeWeapon.GetCost()));
        list.setItems(names);
        Pricelbl.setText(Integer.toString(division.GetPrice()));
    }
}





