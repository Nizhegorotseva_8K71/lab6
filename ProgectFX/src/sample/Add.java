package sample;

import Logic.*;
import javafx.collections.ObservableList;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class Add {
    public void AddObject(Division division, ObservableList names) {
        JTextField NameField = new JTextField();
        JTextField CostField = new JTextField();
        JTextField SharpField = new JTextField();
        JTextField LengthField = new JTextField();
        JTextField SpeedField = new JTextField();
        JTextField DistanceField = new JTextField();
        JTextField PatronField = new JTextField();
        JTextField ScatterField = new JTextField();
        JTextField IncreaseField = new JTextField();

        JTextField[] jTextFields = new JTextField[]{NameField,CostField,SharpField,LengthField,SpeedField,DistanceField,
                PatronField,ScatterField,IncreaseField};

        SpeedField.setEditable(false);
        DistanceField.setEditable(false);
        PatronField.setEditable(false);
        ScatterField.setEditable(false);
        IncreaseField.setEditable(false);

        ButtonGroup group = new ButtonGroup();
        JRadioButton bladeR = new JRadioButton("Blade", true);
        group.add(bladeR);
        JRadioButton sniperR = new JRadioButton("Sniper", false);
        group.add(sniperR);
        JRadioButton shotgunR = new JRadioButton("ShotGun", false);
        group.add(shotgunR);

        JRadioButton[] jRadioButtons = new JRadioButton[]{bladeR,sniperR,shotgunR};

        bladeR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SharpField.setEditable(true);
                LengthField.setEditable(true);
                SpeedField.setEditable(false);
                DistanceField.setEditable(false);
                PatronField.setEditable(false);
                ScatterField.setEditable(false);
                IncreaseField.setEditable(false);
            }
        });
        sniperR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SharpField.setEditable(false);
                LengthField.setEditable(false);
                SpeedField.setEditable(true);
                DistanceField.setEditable(true);
                PatronField.setEditable(true);
                ScatterField.setEditable(false);
                IncreaseField.setEditable(true);
            }
        });
        shotgunR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SharpField.setEditable(false);
                LengthField.setEditable(false);
                SpeedField.setEditable(true);
                DistanceField.setEditable(true);
                PatronField.setEditable(true);
                ScatterField.setEditable(true);
                IncreaseField.setEditable(false);
            }
        });

        Object[] message = new Object[]{"", bladeR, "", sniperR, "", shotgunR, "Name", NameField, "Cost", CostField,
                "Sharp", SharpField, "Length", LengthField, "Speed", SpeedField, "Distance", DistanceField, "Patron"
                , PatronField, "Scatter", ScatterField, "Increase", IncreaseField,"*active fields - necessarily for filling"};

        ShowDialog(message,jTextFields,jRadioButtons,division,names);
    }

    public void ShowDialog( Object[] message,JTextField[] jTextFields, JRadioButton[] jRadioButtons, Division division, ObservableList names) {
        JOptionPane jOptionPane = new JOptionPane();
        int option = jOptionPane.showConfirmDialog(null, message,
                "Create new weapon", JOptionPane.OK_CANCEL_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            String name = jTextFields[0].getText();
            int cost = Integer.parseInt(jTextFields[1].getText());
            if (jRadioButtons[0].isSelected()) {
                int sharp = Integer.parseInt(jTextFields[2].getText());
                int length = Integer.parseInt(jTextFields[3].getText());
                BladeWeapon blade = new BladeWeapon(name, cost, sharp, length);
                division.AddWeapon(blade);
                names.add("Blade: " + blade.GetName() + "; Cost: " + Integer.toString(blade.GetCost()));
            }
            if (jRadioButtons[1].isSelected()) {
                int speed = Integer.parseInt(jTextFields[4].getText());
                int distance = Integer.parseInt(jTextFields[5].getText());
                int patron = Integer.parseInt(jTextFields[6].getText());
                int increase = Integer.parseInt(jTextFields[8].getText());
                Sniper sniper = new Sniper(name, cost, speed, distance, patron, increase);
                division.AddWeapon(sniper);
                names.add("Sniper: " + sniper.GetName() + "; Cost: " + Integer.toString(sniper.GetCost()));
            }
            if (jRadioButtons[2].isSelected()) {
                int speed = Integer.parseInt(jTextFields[4].getText());
                int distance = Integer.parseInt(jTextFields[5].getText());
                int patron = Integer.parseInt(jTextFields[6].getText());
                int scatter = Integer.parseInt(jTextFields[7].getText());
                ShotGun shotGun = new ShotGun(name, cost, speed, distance, patron, scatter);
                division.AddWeapon(shotGun);
                names.add("ShotGun: " + shotGun.GetName() + "; Cost: " + Integer.toString(shotGun.GetCost()));
            }

        } catch (Exception exc) {

            JOptionPane.showMessageDialog(null, "Enter correct values, please!");
            ShowDialog(message,jTextFields,jRadioButtons,division,names);

        }
    }
}

